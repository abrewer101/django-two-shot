from django.shortcuts import render
from django.views.generic import ListView
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404, HttpResponseRedirect
from django.views.generic.edit import CreateView
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.urls import reverse_lazy, reverse


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/receipts_list.html'
    context_object_name = 'receipts'

    def get_queryset(self):
        if not self.request.user.is_authenticated:
            raise Http404("Hey you! Log in, ok? OK?!")
        return Receipt.objects.filter(purchaser=self.request.user)

class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    form_class = ReceiptForm
    template_name = 'receipts/create_receipt.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = 'receipts/category_list.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)

class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = 'receipts/account_list.html'
    context_object_name = 'accounts'

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)

class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    form_class = ExpenseCategoryForm
    template_name = 'receipts/create_category.html'
    success_url = reverse_lazy('category_list')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)

class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    form_class = AccountForm
    template_name = 'receipts/create_account.html'
    success_url = reverse_lazy('account_list')

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
